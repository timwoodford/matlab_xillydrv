function mem_val = xilly_memread(fname, offset)

fh = fopen(fname, 'r');
if fh < 0
    error('Could not open memory file');
end

fseek(fh, 0, 'bof');

mem_val = fread(fh, 1+offset, 'uint8');
mem_val = mem_val(offset+1);

fclose(fh);

end