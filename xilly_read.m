function read_data = xilly_read(streamfile, ctrlfile, num_samples)

% 64-bit packets, 8-bit samples
num_ofxfer = num_samples/64*8;

% Set the number of samples
xilly_memwrite(ctrlfile, 1, bitand(num_ofxfer,255));
xilly_memwrite(ctrlfile, 2, bitand(bitshift(num_ofxfer,-8),255));
xilly_memwrite(ctrlfile, 3, bitand(bitshift(num_ofxfer,-16),255));
xilly_memwrite(ctrlfile, 4, bitand(bitshift(num_ofxfer,-24),255));

% Grab samples
bit_width_setting = xilly_memread('/dev/xillybus_mem_8', 7);
read_data = xilly_fiforead(streamfile, num_samples, bit_width_setting);

if bit_width_setting == 1
    read_data=reshape(flipud(reshape(read_data, 16, length(read_data)/16)), 1, length(read_data));
end

% % Simulate our 8-bit conversion
% read_data = max(read_data, -4095);
% read_data = min(read_data, 4096);
% read_data = floor(read_data/256)*256;

end
