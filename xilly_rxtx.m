function rx_dat = xilly_rxtx(tx_data)
assert(all(abs(tx_data)<=1));

scaled_data=32000*reshape([real(tx_data); imag(tx_data)], 2*length(tx_data), 1);
xilly_prepare_tx(scaled_data);

data = xilly_read('/dev/xillybus_read_128', '/dev/xillybus_mem_8', 2*length(tx_data)+2048);
data = reshape(data, 2, length(data)/2);
data1 = data(1,:); data2=data(2,:);

bit_width_setting = xilly_memread('/dev/xillybus_mem_8', 7);
if bit_width_setting==1
    divisor = 128;
elseif bit_width_setting==2
    divisor = 2048;
end

rx_dat=(-double(data1(300:end)) + 1j*double(data2(300:end)))/divisor;